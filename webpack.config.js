const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlInlineScriptPlugin = require("html-inline-script-webpack-plugin");

module.exports = [
  {
    entry: "./src/index.js",
    mode: "development",
    devServer: {
      contentBase: path.join(__dirname, "dist"),
      compress: true,
      port: 9000,
    },
    output: {
      filename: "main.js",
      path: path.resolve(__dirname, "dist"),
    },
  },
  {
    entry: "./src/inline.js",
    mode: "development",
    plugins: [new HtmlWebpackPlugin({template: "./index.html"}), new HtmlInlineScriptPlugin()],
  },
];
